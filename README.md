
# card game API

Simple REST API for a card game built using java and spring framework

# Install Dependencies

- JDK 11+
 - MongoDB
 - Gradle 

# Usage

Execute following command to run server locally:
> gradle bootRun

Server will automatically listen to port http://localhost:8080

# Routes

| route | method | description |
|--|--|--|--|
| /games | GET | Get list of games |
| /game | PUT | Create a new game |
| /game/{id} | GET | Get game with id |
| /game/{id} | DELETE | Remove game with id |
| /game/{id}/player | PUT | Add player to game with id |
| /game/{id}/player/{playerId} | DELETE | Remove player from game with ids |
| /game/{id}/player/{playerId}/cards | GET | Get player's list of cards |
| /game/{id}/players | GET | Get list of players in game ordered by total value of their deck |
| /game/{id}/deck | PUT | Add deck of 52 cards to game |
| /game/{id}/shoe/shuffle | PUT | Shuffle game's shoe |
| /game/{id}/shoe/cards | GET | Get count of remaining cards in shoe ordered by suits and value from high to low |
| /game/{id}/shoe/suits | GET | Get count of remaining cards by suit in shoe |
| /game/{id}/shoe/deal?playerId={string}&nbCards={int(optional, default=2} | PUT | Deal cards to player with id from game shoe. |
