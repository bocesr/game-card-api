package models;

public class ShoeCard extends Card {

    // Properties
    public int count;

    // Constuctor
    public ShoeCard(short suit, short value) {
        super(suit, value);

        this.count = 1;
    }

    public ShoeCard(byte value) {
        super(value);

        this.count = 1;
    }
}