package models;

import models.Card;

public class Deck {
    
    // Defines
    private final short NB_DECK_CARDS = 52;
    private final short NB_SUIT_CARDS = 13;

    // Properties
    public byte[] cards;

    // Constructor
    public Deck() {
        this.cards = new byte[NB_DECK_CARDS];
        
        this.generateDeck();
    }

    public Deck(byte[] cards) {
        this.cards = cards;
    }

    // Public methods
    public Card[] toCards() {
        Card[] cards = new Card[this.cards.length];

        for (int i = 0; i < this.cards.length; i += 1) {
            cards[i] = new Card(this.cards[i]);
        }
        return cards;
    }

    public int getTotal() {
        int total = 0;

        for (int i = 0; i < this.cards.length; i += 1) {
            Card card = new Card(this.cards[i]);
            total += card.value;
        }
        return total;
    }

    // Private Methods
    private void generateDeck() {
        for (short i = 0; i < NB_DECK_CARDS; i += 1) {
            short suit = (short)(i / NB_SUIT_CARDS + 1);
            short value = (short)(i % NB_SUIT_CARDS + 1);
            Card card = new Card(suit, value);

            this.cards[i] = card.toByte();
        }
    }
}