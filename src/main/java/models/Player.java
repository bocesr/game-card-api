package models;

import org.springframework.data.annotation.Id;

public class Player {

    // Properties
    @Id
    public String id;

    private String gameId;    
    private byte[] cards;
    public int total;

    // Constructor
    public Player(String gameId) {
        this.gameId = gameId;
        this.cards = new byte[]{};
        this.total = 0;
    }

    // Methods
    public void addCards(byte[] cards) {
        byte[] deck = this.cards;
        
        this.cards = new byte[deck.length + cards.length];
        System.arraycopy(deck, 0, this.cards, 0, deck.length);
        System.arraycopy(cards, 0, this.cards, deck.length, cards.length);

        this.updateTotal();
    }

    public byte[] retrieveCards() {
        return this.cards;
    }

    private void updateTotal() {
        Deck deck = new Deck(this.cards);
        this.total = deck.getTotal();
    }
}