package models;

import org.springframework.data.annotation.Id;

public class Game {

    // Properties
    @Id
    public String id;

    private byte[] cards;

    // Constructor
    public Game() {
        this.cards = new byte[]{};
    }

    public Game(byte[] cards) {
        this.cards = cards;
    }

    // Methods
    public void addDeck() {
        Deck deck = new Deck();
        byte[] cards = this.cards;
        int length = this.cards.length + deck.cards.length;
        
        this.cards = new byte[length];
        System.arraycopy(cards, 0, this.cards, 0, cards.length);
        System.arraycopy(deck.cards, 0, this.cards, cards.length, deck.cards.length);
    }

    public byte[] dealCards(int nb) {
        Shoe shoe = new Shoe(this.cards);
        byte[] dealtCards = shoe.shiftCards(nb);

        this.cards = shoe.cards;
        return dealtCards;
    }

    public byte[] retrieveCards() {
        return this.cards;
    }
}