package models;

public class Card {
    
    // Properties
    public short suit;
    public short value;

    // Constructor
    public Card(short suit, short value) {
        this.suit = suit;
        this.value = value;
    }

    public Card(byte value) {
        this.suit = (short)(value >> 4);
        this.value = (short)(value & 15);
    }

    // Methods
    public byte toByte() {
        byte value = (byte)(this.suit << 4);
        return (byte)(value | this.value);
    }
}