package models;

import java.util.Random;

public class Shoe extends Deck {

    // Constructor
    public Shoe(byte[] cards) {
        super(cards);
    }

    // Methods
    public void shuffle() {
        Random random = new Random();

        for (int i = 0; i < this.cards.length; i += 1) {
            int pos = random.nextInt(this.cards.length);
            byte tmp = this.cards[i];

            this.cards[i] = this.cards[pos];
            this.cards[pos] = tmp;
        }
    }

    public byte[] shiftCards(int nbCards) {
        byte[] cards = this.cards;
        int length = Math.max(cards.length - nbCards, 0);
        int nb = cards.length - length;
        byte[] shiftedCards = new byte[nb];

        this.cards = new byte[length];
        System.arraycopy(cards, 0, shiftedCards, 0, nb);
        System.arraycopy(cards, nb, this.cards, 0, length);
        return shiftedCards;
    }
}