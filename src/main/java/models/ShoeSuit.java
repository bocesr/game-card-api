package models;

public class ShoeSuit {

    // Properties
    public short suit;
    public short count;

    // Constructor
    public ShoeSuit(short suit, short count) {
        this.suit = suit;
        this.count = count;
    }
}