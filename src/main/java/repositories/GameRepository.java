package repositories;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import models.Game;
import models.Player;

public interface GameRepository extends MongoRepository<Game, String> {
}