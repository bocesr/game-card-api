package repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import models.Player;

public interface PlayerRepository extends MongoRepository<Player, String> {
    Optional<Player> findByIdAndGameId(String id, String gameId);
    Optional<List<Player>> findAllByGameIdOrderByTotalDesc(String gameId);
    void deleteAllByGameId(String gameId);
}