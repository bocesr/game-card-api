package utils;

import java.util.HashMap;

import models.Card;
import models.ShoeCard;
import models.ShoeSuit;

public class CardUtils {

    // Static Methods
    public static ShoeCard[] countCards(Card[] cards) {
        HashMap<Integer, ShoeCard> map = new HashMap<>();

        for (int i = 0; i < cards.length; i += 1) {
            Card card = cards[i];
            Integer value = (int)card.toByte();
            ShoeCard shoeCard = map.get(value);

            if (shoeCard != null) {
                shoeCard.count += 1;
            } else {
                shoeCard = new ShoeCard(card.suit, card.value);
                map.put(value, shoeCard);
            }
        }
        return map.values().toArray(new ShoeCard[]{});
    }

    public static ShoeSuit[] countSuits(Card[] cards) {
        HashMap<Integer, ShoeSuit> map = new HashMap<>();

        for (int i = 0; i < cards.length; i += 1) {
            Card card = cards[i];
            Integer id = (int)card.suit;
            ShoeSuit suit = map.get(id);

            if (suit != null) {
                suit.count += 1;
            } else {
                suit = new ShoeSuit(card.suit, (short)1);
                map.put(id, suit);
            }
        }
        return map.values().toArray(new ShoeSuit[]{});
    }
}