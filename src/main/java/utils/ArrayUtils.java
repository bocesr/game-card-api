package utils;

public class ArrayUtils {

    // Static methods
    public static <T> T[] reverse(T[] arr) {
        int length = arr.length;

        for (int i = 0; i < length / 2; i += 1) {
            T tmp = arr[i]; 
            arr[i] = arr[length - 1 - i];
            arr[length - 1 - i] = tmp;
        }
        return arr;
    }
}