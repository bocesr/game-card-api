package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import models.Game;
import repositories.GameRepository;

@RestController
@RequestMapping("/games")
public class GamesController {

    @Autowired
    private GameRepository gameRepo;

    @RequestMapping(method = RequestMethod.GET)
    public List<Game> getGames() {
        return gameRepo.findAll();
    }
}
