package controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import models.Player;
import repositories.PlayerRepository;

@RestController
@RequestMapping("/game/{id}/players")
public class PlayersController {

    // Properties
    @Autowired
    private PlayerRepository playerRepo;

    // Methods
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Player>> getPlayers(@PathVariable(value = "id") String id) {
        Optional<List<Player>> players = playerRepo.findAllByGameIdOrderByTotalDesc(id);

        if (players.isPresent()) {
            return new ResponseEntity<List<Player>>(players.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}