package controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import models.Card;
import models.Deck;
import models.Game;
import models.Player;
import repositories.GameRepository;
import repositories.PlayerRepository;

@RestController
@RequestMapping("/game/{id}/player")
class PlayerController {

    // Properties
    @Autowired
    private PlayerRepository playerRepo;

    @Autowired
    private GameRepository gameRepo;

    // Methods
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Player> addPlayer(@PathVariable(value = "id") String id) {
        Optional<Game> optionalGame = gameRepo.findById(id);

        if (optionalGame.isPresent()) {
            Player player = new Player(id);

            playerRepo.save(player);
            return new ResponseEntity<Player>(player, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "{playerId}", method = RequestMethod.DELETE)
    public ResponseEntity deletePlayer(@PathVariable(value = "id") String id,
                                            @PathVariable(value = "playerId") String playerId) {
        Optional<Player> optionalPlayer = playerRepo.findByIdAndGameId(playerId, id);

        if (optionalPlayer.isPresent()) {
            Player player = optionalPlayer.get();

            playerRepo.delete(player);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "{playerId}/cards", method = RequestMethod.GET)
    public ResponseEntity<Card[]> getPlayerCards(@PathVariable(value = "id") String id,
                                            @PathVariable(value = "playerId") String playerId) {
        Optional<Player> optionalPlayer = playerRepo.findByIdAndGameId(playerId, id);

        if (optionalPlayer.isPresent()) {
            Player player = optionalPlayer.get();
            Deck deck = new Deck(player.retrieveCards());

            return new ResponseEntity<Card[]>(deck.toCards(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}