package controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import models.Game;
import repositories.GameRepository;

@RestController
@RequestMapping("/game/{id}/deck")
public class DeckController {

    // Properties
    @Autowired
    private GameRepository gameRepo;

    // Methods
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity addDeck(@PathVariable(value = "id") String id) {
        Optional<Game> optionalGame = gameRepo.findById(id);

        if (optionalGame.isPresent()) {
            Game game = optionalGame.get();

            game.addDeck();
            gameRepo.save(game);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}