package controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import models.Card;
import models.Deck;
import models.Game;
import models.Player;
import models.Shoe;
import models.ShoeCard;
import models.ShoeSuit;
import repositories.GameRepository;
import repositories.PlayerRepository;
import utils.ArrayUtils;
import utils.CardUtils;

@RestController
@RequestMapping("/game/{id}/shoe")
public class ShoeController {

    // Properties
    @Autowired
    private GameRepository gameRepo;

    @Autowired
    private PlayerRepository playerRepo;

    // Methods
    @RequestMapping(value = "/suits", method = RequestMethod.GET)
    public ResponseEntity<ShoeSuit[]> getDeckSuits(@PathVariable(value = "id") String id) {
        Optional<Game> optionalGame = gameRepo.findById(id);

        if (optionalGame.isPresent()) {
            Game game = optionalGame.get();
            Shoe shoe = new Shoe(game.retrieveCards());
            ShoeSuit[] suits = CardUtils.countSuits(shoe.toCards());

            return new ResponseEntity<ShoeSuit[]>(suits, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/cards", method = RequestMethod.GET)
    public ResponseEntity<ShoeCard[]> getDeckCards(@PathVariable(value = "id") String id) {
        Optional<Game> optionalGame = gameRepo.findById(id);

        if (optionalGame.isPresent()) {
            Game game = optionalGame.get();
            Shoe shoe = new Shoe(game.retrieveCards());
            ShoeCard[] cards = CardUtils.countCards(shoe.toCards());

            return new ResponseEntity<ShoeCard[]>(ArrayUtils.reverse(cards), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/shuffle", method = RequestMethod.PUT)
    public ResponseEntity shuffle(@PathVariable(value = "id") String id) {
        Optional<Game> optionalGame = gameRepo.findById(id);

        if (optionalGame.isPresent()) {
            Game game = optionalGame.get();
            Shoe shoe = new Shoe(game.retrieveCards());
            
            shoe.shuffle();
            gameRepo.save(game);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/deal", method = RequestMethod.PUT)
    public ResponseEntity<Card[]> deal(@PathVariable(value = "id") String id,
                               @RequestParam(value = "playerId") String playerId,
                               @RequestParam(value = "nbCards", required = false, defaultValue = "2") String nbCards) {
        Optional<Game> optionalGame = gameRepo.findById(id);
        Optional<Player> optionalPlayer = playerRepo.findByIdAndGameId(playerId, id);
        int nb = Integer.parseInt(nbCards);

        if (optionalGame.isPresent() && optionalPlayer.isPresent()) {
            Game game = optionalGame.get();
            Player player = optionalPlayer.get();
            byte[] dealtCards = game.dealCards(nb);
            Deck deck = new Deck(dealtCards);

            player.addCards(deck.cards);
            playerRepo.save(player);
            gameRepo.save(game);
            return new ResponseEntity<Card[]>(deck.toCards(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } 
    }
}