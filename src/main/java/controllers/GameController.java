package controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import models.Card;
import models.Game;
import models.Player;
import repositories.GameRepository;
import repositories.PlayerRepository;

@RestController
@RequestMapping("/game")
public class GameController {

    @Autowired
    private GameRepository gameRepo;

    @Autowired
    private PlayerRepository playerRepo;

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Game> createGame() {
        Game game = new Game();

        gameRepo.save(game);
        return new ResponseEntity<Game>(game, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Game> getGame(@PathVariable(value = "id") String id) {
        Optional<Game> game = gameRepo.findById(id);

        if (game.isPresent()) {
            return new ResponseEntity<Game>(game.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Game> deleteGame(@PathVariable(value = "id") String id) {
        Optional<Game> game = gameRepo.findById(id);

        if (game.isPresent()) {
            playerRepo.deleteAllByGameId(id);
            gameRepo.delete(game.get());
            return new ResponseEntity<Game>(game.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}