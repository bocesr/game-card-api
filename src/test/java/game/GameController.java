package game;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.jayway.jsonpath.JsonPath;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import models.Game;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GameController {

    @Autowired
    private MockMvc mockMvc;
    
    @Test
    public void createGameShouldReturnGameInstance() throws Exception {
        this.mockMvc.perform(put("/game")).andDo(print()).andExpect(status().isCreated());
    }

    @Test
    public void getGameByShouldReturnGameInstance() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/games")).andExpect(status().isOk()).andReturn();
        String response = result.getResponse().getContentAsString();
        String firstId = JsonPath.parse(response).read("$[0].id");

        this.mockMvc.perform(get("/game/" + firstId)).andExpect(status().isOk()).andExpect(jsonPath("$.id").value(firstId));
    }


    @Test
    public void deleteGame() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/games")).andExpect(status().isOk()).andReturn();
        String response = result.getResponse().getContentAsString();
        String firstId = JsonPath.parse(response).read("$[0].id");

        this.mockMvc.perform(delete("/game/" + firstId)).andExpect(status().isOk());
    }

}